import { dynamicModelGetter } from './dynamicModelGetter';

/**
 * Deletes one by one all documents from collection-type matching the conditions
 * @param {*String} collection one of the collection ["resultCards", "timeslots", "practitioners", "offices"]
 * @param {*String} type one of the medical specialties
 * @param {*Object} identifier object with identifier to satisfy
 */
export const deleteFromType = async ({ collection, type, identifier = {} }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const docs = await Practice.find(identifier).exec();
        return await Promise.all(
            docs.map(async (doc) => {
                await doc.remove();
            })
        );
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not delete all matched documents');
    }
};

export const deleteOneFromType = async ({ collection, type, identifier = {} }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const deleted = await Practice.findOne(identifier);
        await deleted.remove();
        return deleted;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not delete all matched documents');
    }
};
