import dbConnect from '@/common/db/connect.js';

import { dynamicModelGetter } from './dynamicModelGetter';

dbConnect();

/**
 * Return the records with the status sub-attributes updated
 * @param {String} practice
 * @param {keyed Objects} records with each attribute having a sub-attribute "val" and "status"
 */
export const recordsCheck = async (practice, records) => {
    // Import dynamically the attributes based on the constructed name before
    const Practice = await dynamicModelGetter(practice, 'practitioners');
    let newRecords = [];
    await Promise.all(
        records.map(async (record) => {
            newRecords.push(await recordCheck(Practice, record));
        })
    );
    return newRecords;
};

/**
 * Return the record with its "status" sub-attributes updated
 * @param {String} practice
 * @param {keyed Objects} record with each attribute having a sub-attribute "val" and "status"
 */
export const recordCheck = async (Practice, record) => {
    record = nestAttributes(record);
    // 1. Identify if uniquely indentifiable record already exists
    const existingRecord = await recordIdCheck(Practice, record);
    if (existingRecord) {
        // record already exists
        record.id.val = existingRecord._id;
        record.id.status = 'valid';
        record.validity.status = 'valid';
        // check what is invalid in new data
        record = await checkAttrValidity(Practice, record);
        // so check what's new or has changed
        for (const [key, attribute] of Object.entries(record)) {
            if (key === 'validity' || attribute.status === 'invalid') continue;
            if (!existingRecord[key]) {
                if (Object.keys(Practice.schema.paths).includes(key)) {
                    record[key].status = 'new';
                } else record[key].status = 'invalid';
            } else if (existingRecord[key] && existingRecord[key] === attribute.val) {
                record[key].status = 'valid';
            } else {
                record[key].status = 'update';
            }
        }
    } else if (existingRecord == null) {
        // record doesn't exist
        record.id.status = null;
        // Check validity of inserting
        record = await checkAttrValidity(Practice, record);
        for (const key in record)
            if (record[key].status === 'invalid') record.validity.status = 'invalid';
    } else {
        // record is definitely invalid as findOne raised an error
        record.id.status = 'invalid';
        record.validity.status = 'invalid';
    }
    return unNestAttributes(record);
};

/**
 * Return the identified database document based on the record
 * Otherwise, return false if record is invalid or null if it wasn't found
 * @param {Mongoose Model} Practice
 * @param {keyed Object} record
 */
export const recordIdCheck = async (Practice, record) => {
    // Parse indexes from the schema
    const indexes = Practice.schema.indexes();
    // Check for each index if it makes record identifiable
    for (let [attributes] of indexes) {
        let indexObj = {};
        const indexKeys = Object.keys(attributes);
        // Check if key.s of the index are available in the record
        for (let key of indexKeys) {
            if (record[key]) {
                // When available, store the value of the key from the record
                indexObj[key] = record[key].val;
            } else break;
        }
        // Check if record had all the keys needed for that index
        if (Object.keys(indexObj).length === indexKeys.length) {
            // Check if record exists int the database
            try {
                const pract = await Practice.findOne(indexObj).exec();
                if (pract) return pract;
            } catch (error) {
                return false;
            }
        }
    }
    return null;
};

const checkAttrValidity = async (Practice, record) => {
    const newPractitioner = await new Practice(formatRecordToDB(record));
    const validator = newPractitioner.validateSync();
    if (validator && validator.errors) {
        for (const key in validator.errors) {
            if (record[key]) record[key].status = 'invalid';
        }
    }
    return record;
};

/**
 * Below are functions used to help with externalzing small tasks
 */

/**
 * Transform the record to a valid database format - does not mean it is valid
 * @param {keyed object} record
 */
const formatRecordToDB = (record) => {
    var newRecord = {};
    for (const key in record) {
        newRecord[key] = record[key].val;
    }
    return newRecord;
};

/**
 * Multi-nest attributes of the recordthat have been sent as "a.b.c"
 * @param {*} record
 */
const nestAttributes = (record) => {
    for (const key in record) {
        if (key.includes('.')) {
            let keyParts = key.split('.');
            let newVal = nest(keyParts, record[key].val);
            if (!record[keyParts[0]]) {
                record[keyParts[0]] = {
                    val: newVal[keyParts[0]],
                    status: null,
                    previousNames: [key],
                    previousValues: [record[key].val]
                };
            } else {
                record[keyParts[0]].previousNames.push(key);
                record[keyParts[0]].previousValues.push(record[key].val);
                record[keyParts[0]].val = { ...record[keyParts[0]].val, ...newVal[keyParts[0]] };
            }
            delete record[key];
        }
    }
    return record;
};

/**
 * Create a multi-nested object based on the array of keys
 * @param {String array} keys
 * @param {String} value
 */
const nest = (keys, value) => {
    let newVal = {};
    if (keys.length === 1) {
        newVal[keys[0]] = value;
        return newVal;
    } else {
        newVal[keys[0]] = nest(keys.slice(1), value);
        return newVal;
    }
};

/**
 * Retransform the record to fit the names with "." it had previously
 * @param {keyed object} record
 */
const unNestAttributes = (record) => {
    for (const key in record) {
        if (record[key].previousNames) {
            for (const index in record[key].previousNames) {
                record[record[key].previousNames[index]] = {
                    val: record[key].previousValues[index],
                    status: record[key].status
                };
            }
            delete record[key];
        }
    }
    return record;
};

/**
 * Recursive function that enables to take the last value of a multi-nested object
 * @param {object} object
 */
// const unNest = (object) => {
//     const keys = Object.keys(object);
//     if (keys[0] != '0') return unNest(object[keys[0]]);
//     else return object;
// };
