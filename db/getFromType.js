import { dynamicModelGetter } from './dynamicModelGetter';

export const getAllFromType = async ({ collection, type, populate = null }) => {
    const Practice = await dynamicModelGetter(type, collection);
    return await Practice.find({}).populate(populate).exec();
};

export const getAllCount = async ({ collection, type, query = {} }) => {
    const Practice = await dynamicModelGetter(type, collection);
    return await Practice.find(query).count().exec();
};

export const getSomeFromType = async ({ collection, type, query, populate = null }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        return await Practice.find(query).populate(populate).exec();
    } catch (error) {
        throw new Error(error);
    }
};

export const getOneFromType = async ({ collection, type, query, populate = null }) => {
    const Practice = await dynamicModelGetter(type, collection);
    return await Practice.findOne(query).populate(populate).exec();
};

export const getAllAttributesFromType = async ({ collection, type }) => {
    const Practice = await dynamicModelGetter(type, collection);
    const initPaths = Practice.schema.paths;
    let paths = [];
    for (const key of Object.keys(initPaths)) {
        paths.push(key);
        if (initPaths[key].schema && initPaths[key].schema.paths) {
            paths = paths.concat(
                Object.keys(initPaths[key].schema.paths).map((subkey) => {
                    return key + '.' + subkey;
                })
            );
        }
    }
    return paths;
};
