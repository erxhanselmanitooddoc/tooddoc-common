import mongoose from 'mongoose';

import { capitalize } from '@/utils/helpers';

export const dynamicModelGetter = async (practice, sort) => {
    // Formatting the practice received in props ('DENTIST', 'GENERALIST') to 'Dentist' 'Generalist'
    // + the collection (sort) resultCards => ResultCards, offices => Offices
    const resultName = practice
        ? `${capitalize(practice.toLowerCase())}${capitalize(sort)}`
        : // If no practice just get the model as is (eg: 'schedules')
          `${capitalize(sort.toLowerCase())}`;
    // Import dynamically the schema based on the constructed name before
    const schemaAwait = await import(`../models/${resultName}`);
    const schema = schemaAwait.default;
    return mongoose.model(resultName, schema.Model);
};
