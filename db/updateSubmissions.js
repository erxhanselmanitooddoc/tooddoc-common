import UpdateSubmissions from '@/common/models/UpdateSubmissions';

export const createOne = async (practitionerInfos) => {
    try {
        return await UpdateSubmissions.create({
            ...practitionerInfos
        });
    } catch (error) {
        console.error('Error creating a submission =>', error);
        throw new Error('Could not create');
    }
};
export const getAll = async () => {
    try {
        return await UpdateSubmissions.find({}).populate('patient practitioner office');
    } catch (error) {
        console.error('Error get all submissions =>', error);
        throw new Error('Could not create');
    }
};
export const getAllNotTreated = async () => {
    try {
        return await UpdateSubmissions.find({ treated: false }).populate(
            'patient practitioner office'
        );
    } catch (error) {
        console.error('Error get all submissions =>', error);
        throw new Error('Could not create');
    }
};
export const getSome = async ({ query = {} }) => {
    try {
        return await UpdateSubmissions.find(query).populate('patient practitioner office');
    } catch (error) {
        console.error('Error get all submissions =>', error);
        throw new Error('Could not create');
    }
};
export const invalidate = async (id) => {
    try {
        const res = await UpdateSubmissions.updateOne(
            { _id: id },
            { treated: true, validated: false }
        );
        return res.nModified > 0;
    } catch (error) {
        console.error('Error unvalidating a submission =>', error);
        throw new Error('Could not create');
    }
};
export const validate = async (id) => {
    try {
        const res = await UpdateSubmissions.updateOne(
            { _id: id },
            { treated: true, validated: true }
        );
        return res.nModified > 0;
    } catch (error) {
        console.error('Error unvalidating a submission =>', error);
        throw new Error('Could not create');
    }
};
