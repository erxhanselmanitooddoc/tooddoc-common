// import { dynamicModelGetter } from './dynamicModelGetter';

import { getOneFromType, getSomeFromType } from '@/common/db/getFromType';
import { updateFromType } from '@/common/db/updateFromType';
import { distance } from '@/common/utils/distance';

/**
 * Add, if not present yet, the partner attributes to the specified card
 * @param {XXXResultCard} card with at least an attribute _id
 * @param {Object} partnerAttributes corresponding the the Schema for partnerAttributes
 */
export const tagCard = async (card, partnerAttributes) => {
    // Check first if the card already contains this partner
    if (card.partnerAttributes) {
        for (const partner of card.partnerAttributes) {
            if (partner.name === partnerAttributes.name) return 'already existing';
        }
    }
    // Otherwise update in db
    else {
        const result = await updateFromType({
            collection: 'resultCards',
            type: 'DENTIST',
            identifier: {
                _id: card._id,
                partnerAttributes: { $not: { $elemMatch: { name: partnerAttributes.name } } }
            },
            update: { $push: { partnerAttributes } }
        });
        return result;
    }
};

/**
 * FUNCTION BELOW ARE NOT USED (YET)
 */

export const findCorrespondingCard = async (resultCard) => {
    if (!resultCard.practitioner.type || !resultCard.office.geoLocation) return null;
    const type = resultCard.practitioner.type;
    const Prac = await findCorrespondingPrac(resultCard.practitioner, type);
    const Off = await findCorrespondingOffice(resultCard.office, type);
    console.log('Prac ====>', Prac);
    console.log('Off ====>', Off);
};

export const findCorrespondingPrac = async (practitioner, type) => {
    const collection = 'practitioners';
    try {
        if (practitioner.inami)
            return await getOneFromType({ collection, type, query: { inami: practitioner.inami } });

        const results = await getSomeFromType({
            collection,
            type,
            query: {
                firstName: practitioner.firstName,
                lastName: practitioner.lastName
            }
        });
        console.log('results ====>', results);
        // for (const result of results) {
        // }
        return null;
    } catch (error) {
        console.error('Error find corresponding practitioner ===>', error);
        return null;
    }
};
export const findCorrespondingOffice = async (office, type) => {
    // Start with geoLocation
    const collection = 'offices';
    const coordinates = office.geoLocation.coordinates;

    if (office.addressElements) {
        const { streetName, streetNumber, locality, zipCode } = office.addressElements;
        console.log(
            'streetName, streetNumber, locality, zipCode ===>',
            streetName,
            streetNumber,
            locality,
            zipCode
        );
        try {
            const results = await getSomeFromType({
                collection,
                type,
                query: {
                    canonicalAddress: {
                        $regex: `${streetName.substring(0, streetName.lastIndexOf(' '))}`,
                        $options: 'i'
                    }
                }
            });
            for (const result of results) {
                const dist = distance(
                    coordinates[0],
                    coordinates[1],
                    result.geoLocation.coordinates[0],
                    result.geoLocation.coordinates[1],
                    'K'
                );
                if (dist < 1) return result;
            }
            return null;
        } catch (error) {
            console.error('error looking for a corresponding office');
            return null;
        }
    }

    return null;
};
