// var differenceInHours = require('date-fns/difference_in_hours');
import { differenceInHours } from 'date-fns';

/**
 * Gives a weighted score on multiple criteria
 * @param {typeResultCard} resultCard with populated attributes (practitioner, office & timeslots)
 * @param {[lat, long]} coordinates
 */
export const getScore = (resultCard, coordinates) => {
    const { spatialDist, distScore } = compSpatialDistance(resultCard, coordinates);
    const { temporalDist, tempScore } = compTemporalDistance(resultCard);
    const availability = compAvailability(resultCard);
    const exhaustiveness = compExhaustiveness(resultCard);
    return {
        score: (distScore + tempScore + availability + exhaustiveness) * resultCard.weightScore,
        temporalDist,
        spatialDist
    };
};

/**
 * One of the criteria used for the ranking
 * Computes the distance between the card and coordinates and attributes some value between 0 & 1
 * @param {typeResultCard} resultCard with populated attributes (practitioner, office & timeslots)
 * @param {[lat, long]} coordinates
 */
export const compSpatialDistance = (resultCard, coordinates) => {
    const spatialDist = distance(
        resultCard.geoLocation.coordinates[0],
        resultCard.geoLocation.coordinates[1],
        coordinates[0],
        coordinates[1],
        'K'
    );
    let distScore = 0;
    if (spatialDist < 5) distScore = 1;
    else if (spatialDist < 10) distScore = 0.8;
    else if (spatialDist < 15) distScore = 0.6;
    else if (spatialDist < 20) distScore = 0.4;
    else if (spatialDist < 25) distScore = 0.2;
    else distScore = 0;
    return { spatialDist, distScore };
};
/**
 * One of the criteria used for the ranking
 * Computes the # hours between the ealiest timeslot and current time  and gives a score between 0 & 1
 * @param {typeResultCard} resultCard with populated attributes (practitioner, office & timeslots)
 */
export const compTemporalDistance = (resultCard) => {
    const now = new Date();
    const timeslots = resultCard.timeslots.filter((timeslot) => timeslot.date > now);
    var best = 0;
    let temporalDist = 1000;
    if (timeslots.length) {
        temporalDist = differenceInHours(new Date(resultCard?.timeslots[0]?.date), now);
        for (const timeslot of timeslots) {
            let diff = differenceInHours(new Date(timeslot.date), now);
            if (diff < 6) best = 1;
            else if (diff < 12) best = Math.max(best, 0.8);
            else if (diff < 24) best = Math.max(best, 0.6);
            else if (diff < 48) best = Math.max(best, 0.4);
            else best = Math.max(best, 0.2);
        }
    }

    return { tempScore: best, temporalDist };
};

/**
 * One of the criteria used for the ranking
 * Computes the amount of timeslots available (with max 10) and gives a score between 0 & 1
 * @param {typeResultCard} resultCard with populated attributes (practitioner, office & timeslots)
 */
export const compAvailability = (resultCard) => {
    const max = 10;
    return resultCard.timeslots.length > 0 ? Math.min(resultCard.timeslots.length, max) / 10 : 0;
};

/**
 * One of the criteria used for the ranking
 * Computes the amount of data points / info on the office and gives a score between 0 & 1
 * @param {typeResultCard} resultCard with populated attributes (practitioner, office & timeslots)
 */
export const compExhaustiveness = (resultCard) => {
    var sum = 0;
    var max = 10;
    if (resultCard?.office?.officeElements) {
        for (const key in resultCard.office.officeElements) {
            if (
                resultCard.office.officeElements[key] &&
                resultCard.office.officeElements[key] != ''
            )
                sum++;
        }
    }
    if (resultCard?.office?.contactElements) {
        if (resultCard.office.contactElements.phone) sum = sum + 4;
        if (resultCard.office.contactElements.email) sum = sum + 3;
        if (resultCard.office.contactElements.website) sum = sum + 2;
    }
    return Math.min(sum, max) / max;
};

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2018            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2) {
        return 0;
    } else {
        var radlat1 = (Math.PI * lat1) / 180;
        var radlat2 = (Math.PI * lat2) / 180;
        var theta = lon1 - lon2;
        var radtheta = (Math.PI * theta) / 180;
        var dist =
            Math.sin(radlat1) * Math.sin(radlat2) +
            Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = (dist * 180) / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        }
        if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return dist;
    }
}
