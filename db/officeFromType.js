import { createManyFromType } from '@/common/db/createFromType';
import { getAllAttributesFromType, getOneFromType } from '@/common/db/getFromType';

const toExclude = ['practitionersIds'];
// const batchSize = 50;

export const createOffices = async ({ practice, allInstances }) => {
    try {
        // let newIndex = 0;
        // Divide bulk insert operations in batches
        let newInstances = [];
        for (const instance of allInstances) {
            newInstances.push(await transformToSchema(instance, practice));
        }
        const promisedInstances = await Promise.all(newInstances);
        await createManyFromType({
            collection: 'offices',
            type: practice,
            instances: promisedInstances
        });
        // for (let index = 0; index < allInstances.length; index = index + batchSize) {
        //     // The instances used in this batch
        //     let instances = allInstances.slice(
        //         index,
        //         Math.min(index + batchSize, allInstances.length)
        //     );
        //     // Transform instances to insertable schemas
        //     await Promise.all(
        //         instances.map(async (instance) => {
        //             newInstances.push(await transformToSchema(instance, practice));
        //         })
        //     );

        //     // Create in database using create many (Mongoose insertMany)
        //     // Make sure no office has similar lat & long than another one
        //     // for (const inst of newInstances) {
        //     //     newIndex++;
        //     //     console.log('inst.canonicalAddress ===>', newIndex, inst.canonicalAddress);
        //     // }
        //     await createManyFromType({
        //         collection: 'offices',
        //         type: practice,
        //         instances: newInstances
        //     });
        // }
        return { instances: newInstances, length: newInstances.length };
    } catch (error) {
        console.error('createOffices error ===>', error);
        throw new Error(error);
    }
};

/**
 * Return an object with attributes corresponding to the schema type
 * @param {Object} initialObject
 * @param {String} practice
 */
const transformToSchema = async (initialObject, practice) => {
    try {
        let transformedObject = {}; // the new object
        let allAttr = await getAllAttributesFromType({
            collection: 'offices',
            type: practice
        });
        for (const attribute of allAttr) {
            if (initialObject[attribute] && !toExclude.includes(attribute)) {
                transformedObject[attribute.split('.')[0]] = initialObject[attribute];
            }
            // Construct geoLocation sub-object
            if (attribute === 'geoLocation') {
                transformedObject[attribute].coordinates = JSON.parse(
                    transformedObject[attribute].coordinates
                );
                transformedObject[attribute].type = 'Point';
            }
            // If practitioner not found there will be a "null" value in the array - shouldn't happen
            // Check on mongo if there are some {"practitionersIds":{$elemMatch:{"$in":[null], "$exists":true}}}
            if (attribute === 'practitionersIds') {
                // Based on inami hence not valid for all
                let inamis = initialObject[attribute].split(',');
                let newIds = await Promise.all(
                    inamis.map(async (inami) => {
                        const parsedInami = parseInt(inami, 10);
                        if (!isNaN(parsedInami)) {
                            const prac = await getOneFromType({
                                collection: 'practitioners',
                                type: practice,
                                query: { 'inamiAttributes.inami': inami }
                            });
                            return prac?._id;
                        }
                    })
                );
                transformedObject[attribute] = newIds;
            }
        }
        return transformedObject;
    } catch (error) {
        console.log('transformToSchema error ====>', error);
        throw new Error(error);
    }
};
