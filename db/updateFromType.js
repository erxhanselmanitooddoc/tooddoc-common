import { dynamicModelGetter } from './dynamicModelGetter';

export const updateOneFromType = async ({
    collection,
    type,
    identifier,
    update,
    specialOp = null,
    populate = null
}) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const doc = await Practice.findOne(identifier).populate(populate).exec();
        for (const key in update) {
            let val = getFromKey(doc, key); // checking for nested
            if (Array.isArray(val)) {
                if (specialOp === 'pull') val.pull(update[key]);
                else {
                    // Checking duplicates eg in resultCard.partnerAttributes
                    if (val.length > 0 && val[0].name) {
                        if (val.map((elem) => elem.name).indexOf(update[key].name) == -1)
                            val.push(update[key]);
                    }
                    //Checking duplicates eg in practitioner.officeIds
                    else {
                        if (val.indexOf(update[key]) == -1) val.push(update[key]);
                    }
                }
            } else {
                val = update[key];
            }
        }
        await doc.save();
        return doc;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};

const getFromKey = (doc, key) => {
    const subPaths = key.split('.');
    switch (subPaths.length) {
        case 2:
            return doc[subPaths[0]][subPaths[1]];
        case 3:
            return doc[subPaths[0]][subPaths[1]][subPaths[2]];
        case 4:
            return doc[subPaths[0]][subPaths[1]][subPaths[2]][subPaths[3]];
        default:
            return doc[key];
    }
};

export const updateFromType = async ({
    collection,
    type,
    identifier = {},
    update,
    populate = null
}) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const result = await Practice.updateOne(identifier, update, { new: true })
            .populate(populate)
            .exec();
        return result;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};

export const updateManyFromType = async ({
    collection,
    type,
    identifier = {},
    update,
    populate = null
}) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const result = await Practice.updateMany(identifier, update, { new: true })
            .populate(populate)
            .exec();
        return result;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};

export const updateOneForType = async ({
    collection,
    type,
    identifier,
    update,
    populate = null
}) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        const result = await Practice.findOneAndUpdate(identifier, update, { new: true }).populate(
            populate
        );
        if (!result) throw new Error('Could not update');
        return result;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};
