import { connect, connection } from 'mongoose';

const dbConnect = async () => {
    try {
        if (connection.readyState === 1) {
            return;
        }

        await connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    } catch (error) {
        connection.close();
        console.error('Error connecting DB ====>', error);
    }
};

export default dbConnect;
