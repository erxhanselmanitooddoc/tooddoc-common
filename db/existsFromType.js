import { dynamicModelGetter } from './dynamicModelGetter';

export const existsFromType = async ({ collection, type, identifier }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        return await Practice.exists(identifier);
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Cant check if it exists or not');
    }
};
