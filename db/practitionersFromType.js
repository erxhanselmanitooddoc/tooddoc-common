import { createManyFromType } from '@/common/db/createFromType';
import { getAllAttributesFromType } from '@/common/db/getFromType';

const toExclude = ['officeIds', 'inamiAttributes'];

export const createPractitioners = async ({ practice, instances }) => {
    try {
        // Create instances that will fit the schema
        let newInstances = [];
        for (const instance of instances) {
            newInstances.push(await transformToSchema(instance, practice));
        }

        // Create in database using create many (Mongoose insertMany)
        const result = await createManyFromType({
            collection: 'practitioners',
            type: practice,
            instances: newInstances
        });

        return result;
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};

/**
 * Return an object with attributes corresponding to the schema type
 * @param {Object} initialObject
 * @param {String} practice
 */
const transformToSchema = async (initialObject, practice) => {
    try {
        let transformedObject = {}; // the new object
        let allAttr = await getAllAttributesFromType({
            collection: 'practitioners',
            type: practice
        });
        for (const attribute of allAttr) {
            if (initialObject[attribute]) {
                /* If the subOjbects are already constructed use as below */
                if (!toExclude.includes(attribute))
                    transformedObject[attribute.split('.')[0]] = initialObject[attribute];

                if (attribute === 'inamiAttributes') {
                    transformedObject[attribute.split('.')[0]] = initialObject[attribute];
                    const dateFormat =
                        initialObject['inamiAttributes']['inamiStartDate'].split('/');
                    transformedObject['inamiAttributes']['inamiStartDate'] = new Date(
                        dateFormat[2],
                        dateFormat[1],
                        dateFormat[0]
                    );
                }

                /* If the subAttributes are sent as "tooddocAttributes.isCertified" then use below */
                // const subAttrs = attribute.split('.'); // [tooddoc, attr, attr2]
                // createNestedObject(transformedObject, subAttrs, initialObject[attribute]);
            }
        }
        return transformedObject;
    } catch (error) {
        console.error('error ====>', error);
        throw new Error(error);
    }
};

// Function: createNestedObject( base, names[, value] )
//   base: the object on which to create the hierarchy
//   names: an array of strings contaning the names of the objects
//   value (optional): if given, will be the last object in the hierarchy
// Returns: the last object in the hierarchy
// var createNestedObject = function (base, names, value) {
//     // If a value is given, remove the last name and keep it for later:
//     var lastName = arguments.length === 3 ? names.pop() : false;

//     // Walk the hierarchy, creating new objects where needed.
//     // If the lastName was removed, then the last object is not set yet:
//     for (var i = 0; i < names.length; i++) {
//         base = base[names[i]] = base[names[i]] || {};
//     }

//     // If a value was given, set it to the last name:
//     if (lastName) base = base[lastName] = value;

//     // Return the last object in the hierarchy:
//     return base;
// };
