import { dynamicModelGetter } from './dynamicModelGetter';

/**
 * Firing Mongoose validation and 'save' hooks
 * @param {Object} param0 that has ready-made (insertable) instances of the type & collection, with optional populate string with fields to populate
 */
export const createOneFromType = async ({ collection, type, instance, populate = null }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        if (populate)
            return await (await Practice.create(instance))
                .populate('practitioner office')
                .execPopulate();
        else return await Practice.create(instance);
    } catch (error) {
        console.error('error ===>', error);
        throw new Error(error);
    }
};

/**
 * Use Mongoose insertMany function that run each document through validation but doesn't run through hooks (except insertMany hooks)
 * @param {Object} param0 that has ready-made (insertable) instances of the type & collection
 * Return error if any of the documents fail validation
 */
export const createManyFromType = async ({ collection, type, instances }) => {
    const Practice = await dynamicModelGetter(type, collection);
    return await Practice.insertMany(instances);
};

/**
 * Ideally it should NOT be used
 * Bypass Mongoose and uses direct Mongo collection function, hence no validation nor hook is used
 * @param {Object} param0 that has ready-made (insertable) instances of the type & collection
 */
export const createBulkFromType = async ({ collection, type, instances }) => {
    try {
        const Practice = await dynamicModelGetter(type, collection);
        var bulk = Practice.collection.initializeUnorderedBulkOp();
        await instances.forEach((element) => {
            bulk.insert(element);
        });
        bulk.execute(function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    } catch (error) {
        console.error('error ===>', error);
        throw new Error('Could not update');
    }
};
