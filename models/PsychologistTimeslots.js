const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PsychologistOffices = require('./PsychologistOffices.js');
const PsychologistPractitioners = require('./PsychologistPractitioners.js');
const Appointments = require('./Appointments');

import { updateOneFromType } from '@/common/db/updateFromType';

const PsychologistTimeslotsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: PsychologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: PsychologistOffices
        },
        appointment: {
            type: mongoose.Schema.Types.ObjectId,
            ref: Appointments
        },
        time: {
            type: Number,
            required: [true, 'Why no time?'],
            min: 0,
            max: 86400
        },
        date: {
            type: Date,
            required: [true, 'Why no date?'],
            min: new Date()
        },
        duration: {
            type: Number,
            required: [true, 'Why no duration?'],
            default: '15',
            min: 1,
            max: 240
        },
        removedFromResultCard: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    }
);

PsychologistTimeslotsSchema.index(
    { practitioner: 1, office: 1, date: 1, time: 1 },
    { unique: true, sparse: true }
);

PsychologistTimeslotsSchema.virtual('startTime').get(function () {
    return this.date;
});

PsychologistTimeslotsSchema.virtual('endTime').get(function () {
    return new Date(this.date.getTime() + this.duration * 60000);
});

/**
 * Middlewares to ensure validation & consistency of references elsewhere
 */

PsychologistTimeslotsSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});
// Document middleware, is also called when using Model.create
PsychologistTimeslotsSchema.post('save', function (doc) {
    try {
        updateOneFromType({
            collection: 'resultCards',
            type: 'PSYCHOLOGIST',
            identifier: { practitioner: doc.practitioner, office: doc.office },
            update: { timeslots: doc._id }
        });
    } catch (error) {
        throw new Error('Could not update corresponding resultCard');
    }
});
// Document middleware (model needs {query: true})
PsychologistTimeslotsSchema.post('remove', { document: true }, function () {
    try {
        updateOneFromType({
            collection: 'resultCards',
            type: 'PSYCHOLOGIST',
            identifier: { practitioner: this.practitioner, office: this.office },
            update: { $pull: { timeslots: this._id } }
        });
    } catch (error) {
        throw new Error('Could not update corresponding resultCard');
    }
});

module.exports =
    mongoose?.models?.PsychologistTimeslots ??
    mongoose.model('PsychologistTimeslots', PsychologistTimeslotsSchema);
