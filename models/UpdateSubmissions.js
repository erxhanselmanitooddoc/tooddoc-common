const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const practices = require('@/common/utils/practices.json');
import { capitalize } from '@/utils/helpers';

const Patients = require('./Patients');

const UpdateSubmissionsSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: practices,
            uppercase: true,
            trim: true
        },
        practitionerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        officeId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        changes: {
            type: mongoose.Schema.Types.Mixed,
            required: true
        },
        submissionTime: {
            type: Date,
            required: true,
            default: Date.now
        },
        patient: {
            type: mongoose.Schema.Types.ObjectId,
            // required: true,
            ref: Patients
        },
        validated: {
            type: Boolean,
            required: true,
            default: false
        },
        treated: {
            type: Boolean,
            required: true,
            default: false
        },
        wantDeletion: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true,
        toObject: { virtuals: true },
        toJSON: { virtuals: true }
    }
);

/**
 * Virtual populates for practitioner, office & timeslot
 */
UpdateSubmissionsSchema.virtual('practitioner', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Practitioners';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'practitionerId',
    foreignField: '_id',
    justOne: true
});
UpdateSubmissionsSchema.virtual('office', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Offices';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'officeId',
    foreignField: '_id',
    justOne: true
});
const getDynamicModel = (resultName) => {
    return require(`../models/${resultName}`).default;
};

UpdateSubmissionsSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.UpdateSubmissions ??
    mongoose.model('UpdateSubmissions', UpdateSubmissionsSchema);
