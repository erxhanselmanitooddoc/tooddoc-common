const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * timeRange represents opening hours from a starting second to an ending second
 * second 0 = 00:00:00 while second 86399 = 23:59:59
 */
const ScheduleTimeRangeSchema = new Schema(
    {
        start: {
            type: Number,
            required: true,
            default: -1,
            min: -1,
            max: 86400
        },
        end: {
            type: Number,
            required: true,
            default: -1,
            min: -1,
            max: 86400
        }
    },
    { _id: false }
);

module.exports = ScheduleTimeRangeSchema;
