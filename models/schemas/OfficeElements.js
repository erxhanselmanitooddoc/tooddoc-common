const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// SubDocument for practitioners that are part of tooddoc
const officeElementsSchema = new Schema(
    {
        acceptCreditCard: {
            type: Boolean,
            default: null
        },
        acceptBancontact: {
            type: Boolean,
            default: null
        },
        acceptCash: {
            type: Boolean,
            default: null
        },
        acceptQRCode: {
            type: Boolean,
            default: null
        },
        wheelchairAccessible: {
            type: Boolean,
            default: null
        }
    },
    { timestamps: true, _id: false }
);

module.exports = officeElementsSchema;
