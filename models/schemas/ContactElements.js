const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// SubDocument for practitioners that are part of tooddoc
const contactElementsSchema = new Schema(
    {
        phone: Number,
        email: {
            type: String,
            lowercase: true,
            trim: true
        },
        website: {
            type: String,
            lowercase: true,
            trim: true
        }
    },
    { timestamps: true, _id: false }
);

module.exports = contactElementsSchema;
