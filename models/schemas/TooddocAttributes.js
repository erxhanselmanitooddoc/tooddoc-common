const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// SubDocument for practitioners that are part of tooddoc
const TooddocAttributesSchema = new Schema(
    {
        isCertified: {
            type: Boolean,
            required: [true, 'Is this practitioner certified ?'],
            default: false
        },
        contactEmail: {
            // TODO: adding an email string validator
            type: String,
            lowercase: true
        },
        previousId: {
            type: String
        },
        dateTimeCreated: {
            type: Date
        }
    },
    { timestamps: true, _id: false }
);

module.exports = TooddocAttributesSchema;
