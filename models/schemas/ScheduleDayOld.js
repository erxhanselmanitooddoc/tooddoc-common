const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleDayOldSchema = new Schema(
    {
        isActive: {
            type: Boolean,
            default: false,
            required: true
        },
        timeRanges: {
            type: [
                {
                    start: {
                        type: Number,
                        required: true,
                        default: -1,
                        min: -1,
                        max: 97
                    },
                    end: {
                        type: Number,
                        required: true,
                        default: -1,
                        min: -1,
                        max: 97
                    }
                }
            ],
            required: true
        }
    },
    { _id: false }
);

module.exports = ScheduleDayOldSchema;
