const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PartnerAttributesSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, 'Which partner does it come from?'],
            enum: ['Doctena', 'DoctorAnyTime', 'MyConsultation', 'AdHoc'],
            trim: true
        },
        link: {
            type: String,
            trim: true
        },
        practitionerId: String,
        isBookingPossible: Boolean
    },
    { timestamps: true, _id: false }
);

module.exports = PartnerAttributesSchema;
