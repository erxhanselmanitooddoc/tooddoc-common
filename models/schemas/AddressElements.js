const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const addressElementsSchema = new Schema(
    {
        country: {
            type: String,
            required: true,
            trim: true,
            default: 'Belgium',
            enum: ['Belgium', 'Netherlands', 'France', 'Germany', 'Luxembourg']
        },
        locality: {
            type: String,
            required: true,
            trim: true
        },
        zipCode: {
            type: Number,
            required: true,
            trim: true
        },
        streetName: {
            type: String,
            required: true,
            trim: true
        },
        streetNumber: {
            type: String,
            trim: true
        }
    },
    {
        _id: false
    }
);

module.exports = addressElementsSchema;
