const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GeoJSONPointSchema = new Schema(
    {
        type: {
            type: String,
            enum: ['Point'],
            default: 'Point',
            required: true
        },
        coordinates: {
            type: [Number],
            // set: coordinatesFormator,
            validate: [coordinatesValidator, 'These coordinates are not valid'],
            required: [true, 'Why no coordinates ?']
        }
    },
    { autoIndex: true, _id: false }
);

GeoJSONPointSchema.index({ 'coordinates.0': 1, 'coordinates.1': 1 }, { unique: true });

function coordinatesValidator(val) {
    return val.length === 2 && val.every((item) => item?.toString().split('.').length === 2);
}

module.exports = GeoJSONPointSchema;
