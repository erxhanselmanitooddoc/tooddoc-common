const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleTimeRangeSchema = require('./ScheduleTimeRange');

const ScheduleDayNewSchema = new Schema(
    {
        isActive: {
            type: Boolean,
            default: false,
            required: true
        },
        timeRanges: {
            type: [ScheduleTimeRangeSchema],
            required: [true, 'Why no timeRanges?']
        }
    },
    { _id: false }
);

module.exports = ScheduleDayNewSchema;
