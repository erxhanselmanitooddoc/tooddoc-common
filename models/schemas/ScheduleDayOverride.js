const mongoose = require('mongoose');
const ScheduleTimeRangeSchema = require('./ScheduleTimeRange');
const Schema = mongoose.Schema;

const ScheduleDayOverrideSchema = new Schema(
    {
        day: {
            type: Date,
            required: [true, 'Why no date?'],
            min: new Date()
        },
        timeRanges: {
            type: [ScheduleTimeRangeSchema],
            required: [true, 'Why no timeRanges?']
        }
    },
    { _id: false }
);

module.exports = ScheduleDayOverrideSchema;
