const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const languages = require('@/common/utils/languages.json');

const personAttributesSchema = new Schema(
    {
        firstName: {
            type: String,
            required: [true, 'Why no first name ?'],
            trim: true
        },
        lastName: {
            type: String,
            required: [true, 'Why no last name ?'],
            trim: true
        },
        sex: {
            type: String,
            required: true,
            enum: ['M', 'F', 'B'],
            default: 'M',
            uppercase: true,
            trim: true
        },
        avatar: {
            type: String,
            required: true,
            default: 'default',
            trim: true
        },
        profilePicture: {
            // TODO: create validator for URL link toward picture; self hosted ?
            type: String,
            trim: true
        },
        languages: [{ type: String, enum: languages }],
        hasRetired: {
            type: Boolean,
            default: false
        },
        keptHidden: {
            type: Boolean,
            default: false
        }
    },
    { _id: false }
);

module.exports = personAttributesSchema;
