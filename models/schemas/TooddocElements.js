const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// SubDocument for offices that are part of tooddoc
const TooddocElementsSchema = new Schema(
    {
        isAlertEmailEnabled: {
            type: Boolean,
            default: false
        },
        previousDoctorId: {
            type: [String]
        },
        previousId: {
            type: [String]
        }
    },
    { _id: false }
);

module.exports = TooddocElementsSchema;
