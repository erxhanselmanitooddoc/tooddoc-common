const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const inamiAttributesSchema = ({ min, max }) =>
    new Schema(
        {
            inami: {
                type: Number,
                required: true,
                unique: true,
                min: [min, 'Invalid INAMI number'],
                max: [max, 'Invalid INAMI number']
            },
            inamiStatus: {
                type: String,
                enum: ['fully covered', 'partially covered', 'not covered'],
                default: 'not covered',
                lowercase: true,
                trim: true
            },
            inamiStartDate: {
                type: Date,
                max: new Date()
            }
        },
        { _id: false }
    );

module.exports = inamiAttributesSchema;
