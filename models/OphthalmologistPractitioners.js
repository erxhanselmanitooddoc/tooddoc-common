const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const OphthalmologistOffices = require('./OphthalmologistOffices.js');

const OphthalmologistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'OPHTHALMOLOGIST',
            enum: ['OPHTHALMOLOGIST'],
            uppercase: true,
            trim: true
        },
        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: OphthalmologistOffices }]
    },
    {
        timestamps: true
    }
);

OphthalmologistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.OphthalmologistPractitioners ??
    mongoose.model('OphthalmologistPractitioners', OphthalmologistPractitionersSchema);
