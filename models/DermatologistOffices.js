const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const geoJSONPointSchema = require('./schemas/GeoJSONPoint.js');
const officeElementsSchema = require('./schemas/OfficeElements.js');
const tooddocElementsSchema = require('./schemas/TooddocElements');
const DermatologistPractitioners = require('./DermatologistPractitioners.js');
const contactElementsSchema = require('./schemas/ContactElements.js');
const addressElementsSchema = require('./schemas/AddressElements.js');

const DermatologistOfficesSchema = new Schema(
    {
        geoLocation: {
            type: geoJSONPointSchema,
            required: true,
            index: '2dsphere'
        },
        canonicalAddress: {
            type: String,
            unique: true,
            required: true,
            trim: true
        },
        name: {
            type: String,
            trim: true
        },
        addressElements: addressElementsSchema,
        contactElements: contactElementsSchema,
        practitionersIds: [
            { type: mongoose.Schema.Types.ObjectId, ref: DermatologistPractitioners }
        ],
        officeElements: officeElementsSchema,
        tooddocElements: tooddocElementsSchema
    },
    {
        timestamps: true
    }
);

DermatologistOfficesSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.DermatologistOffices ??
    mongoose.model('DermatologistOffices', DermatologistOfficesSchema);
