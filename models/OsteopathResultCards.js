const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OsteopathOffices = require('./OsteopathOffices.js');
const OsteopathPractitioners = require('./OsteopathPractitioners.js');
const OsteopathTimeslots = require('./OsteopathTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const OsteopathResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: OsteopathPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: OsteopathOffices
        },
        type: {
            type: String,
            required: true,
            default: 'OSTEOPATH',
            enum: ['OSTEOPATH'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: OsteopathTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

OsteopathResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.OsteopathResultCards ??
    mongoose.model('OsteopathResultCards', OsteopathResultCardsSchema);
