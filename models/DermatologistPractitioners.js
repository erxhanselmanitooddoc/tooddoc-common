const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const DermatologistOffices = require('./DermatologistOffices.js');

const DermatologistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'DERMATOLOGIST',
            enum: ['DERMATOLOGIST'],
            uppercase: true,
            trim: true
        },
        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: DermatologistOffices }]
    },
    {
        timestamps: true
    }
);

DermatologistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.DermatologistPractitioners ??
    mongoose.model('DermatologistPractitioners', DermatologistPractitionersSchema);
