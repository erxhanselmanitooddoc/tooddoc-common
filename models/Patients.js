const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// const languages = require('@/common/utils/languages.json');

const PatientsSchema = new Schema(
    {
        firstName: {
            type: String,
            // required: [true, 'Why no first name ?'],
            trim: true
        },
        lastName: {
            type: String,
            // required: [true, 'Why no last name ?'],
            trim: true
        },
        email: {
            type: String,
            required: [true, 'Email is missing'],
            unique: true,
            trim: true
        },
        phone: {
            type: String,
            unique: true,
            trim: true,
            sparse: true
        },
        languages: [{ type: String }],
        age: {
            type: Number,
            trim: true
        }
    },
    {
        timestamps: true
    }
);

PatientsSchema.virtual('fullName').get(function () {
    return this.firstName + ' ' + this.lastName;
});

PatientsSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports = mongoose?.models?.Patients ?? mongoose.model('Patients', PatientsSchema);
