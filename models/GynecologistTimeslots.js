const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GynecologistOffices = require('./GynecologistOffices.js');
const GynecologistPractitioners = require('./GynecologistPractitioners.js');
const Appointments = require('./Appointments');

import { updateOneFromType } from '@/common/db/updateFromType';

const GynecologistTimeslotsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: GynecologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: GynecologistOffices
        },
        appointment: {
            type: mongoose.Schema.Types.ObjectId,
            ref: Appointments
        },
        time: {
            type: Number,
            required: [true, 'Why no time?'],
            min: 0,
            max: 86400
        },
        date: {
            type: Date,
            required: [true, 'Why no date?'],
            min: new Date()
        },
        duration: {
            type: Number,
            required: [true, 'Why no duration?'],
            default: '15',
            min: 1,
            max: 240
        },
        removedFromResultCard: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    }
);

GynecologistTimeslotsSchema.index(
    { practitioner: 1, office: 1, date: 1, time: 1 },
    { unique: true, sparse: true }
);

GynecologistTimeslotsSchema.virtual('startTime').get(function () {
    return this.date;
});

GynecologistTimeslotsSchema.virtual('endTime').get(function () {
    return new Date(this.date.getTime() + this.duration * 60000);
});

/**
 * Middlewares to ensure validation & consistency of references elsewhere
 */

GynecologistTimeslotsSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});
// Document middleware, is also called when using Model.create
GynecologistTimeslotsSchema.post('save', function (doc) {
    try {
        updateOneFromType({
            collection: 'resultCards',
            type: 'GYNECOLOGIST',
            identifier: { practitioner: doc.practitioner, office: doc.office },
            update: { timeslots: doc._id }
        });
    } catch (error) {
        throw new Error('Could not update corresponding resultCard');
    }
});
// Document middleware (model needs {query: true})
GynecologistTimeslotsSchema.post('remove', { document: true }, function () {
    try {
        updateOneFromType({
            collection: 'resultCards',
            type: 'GYNECOLOGIST',
            identifier: { practitioner: this.practitioner, office: this.office },
            update: { $pull: { timeslots: this._id } }
        });
    } catch (error) {
        throw new Error('Could not update corresponding resultCard');
    }
});

module.exports =
    mongoose?.models?.GynecologistTimeslots ??
    mongoose.model('GynecologistTimeslots', GynecologistTimeslotsSchema);
