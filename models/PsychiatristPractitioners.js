const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const PsychiatristOffices = require('./PsychiatristOffices.js');

const PsychiatristPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'PSYCHIATRIST',
            enum: ['PSYCHIATRIST'],
            uppercase: true,
            trim: true
        },

        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: PsychiatristOffices }]
    },
    {
        timestamps: true
    }
);

PsychiatristPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.PsychiatristPractitioners ??
    mongoose.model('PsychiatristPractitioners', PsychiatristPractitionersSchema);
