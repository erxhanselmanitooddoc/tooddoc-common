const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const PsychologistOffices = require('./PsychologistOffices.js');

const PsychologistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 90000000, max: 99999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'PSYCHOLOGIST',
            enum: ['PSYCHOLOGIST'],
            uppercase: true,
            trim: true
        },

        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: PsychologistOffices }]
    },
    {
        timestamps: true
    }
);

PsychologistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.PsychologistPractitioners ??
    mongoose.model('PsychologistPractitioners', PsychologistPractitionersSchema);
