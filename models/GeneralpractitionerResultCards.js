const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GeneralpractitionerOffices = require('./GeneralpractitionerOffices.js');
const GeneralpractitionerPractitioners = require('./GeneralpractitionerPractitioners.js');
const GeneralpractitionerTimeslots = require('./GeneralpractitionerTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const GeneralpractitionerResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: GeneralpractitionerPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: GeneralpractitionerOffices
        },
        type: {
            type: String,
            required: true,
            default: 'GENERALPRACTITIONER',
            enum: ['GENERALPRACTITIONER'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: GeneralpractitionerTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

GeneralpractitionerResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.GeneralpractitionerResultCards ??
    mongoose.model('GeneralpractitionerResultCards', GeneralpractitionerResultCardsSchema);
