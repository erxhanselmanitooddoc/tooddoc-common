const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const GeneralpractitionerOffices = require('./GeneralpractitionerOffices.js');

const GeneralpractitionerPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'GENERALPRACTITIONER',
            enum: ['GENERALPRACTITIONER'],
            uppercase: true,
            trim: true
        },
        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: GeneralpractitionerOffices }]
    },
    {
        timestamps: true
    }
);

GeneralpractitionerPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.GeneralpractitionerPractitioners ??
    mongoose.model('GeneralpractitionerPractitioners', GeneralpractitionerPractitionersSchema);
