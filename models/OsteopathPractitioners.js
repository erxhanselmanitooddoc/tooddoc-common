const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const OsteopathOffices = require('./OsteopathOffices.js');

const OsteopathPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 80000000, max: 89999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        isMemberOfOsteopathie: {
            type: Boolean,
            required: true,
            default: false
        },
        type: {
            type: String,
            required: true,
            default: 'OSTEOPATH',
            enum: ['OSTEOPATH'],
            uppercase: true,
            trim: true
        },

        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: OsteopathOffices }]
    },
    {
        timestamps: true
    }
);

OsteopathPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.OsteopathPractitioners ??
    mongoose.model('OsteopathPractitioners', OsteopathPractitionersSchema);
