const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// SubDocuments for practitioners that are part of tooddoc & those from partner websites
const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const PhysiotherapistOffices = require('./PhysiotherapistOffices.js');

const PhysiotherapistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 50000000, max: 59999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'PHYSIOTHERAPIST',
            enum: ['PHYSIOTHERAPIST'],
            uppercase: true,
            trim: true
        },

        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: PhysiotherapistOffices }]
    },
    {
        timestamps: true
    }
);

PhysiotherapistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.PhysiotherapistPractitioners ??
    mongoose.model('PhysiotherapistPractitioners', PhysiotherapistPractitionersSchema);
