const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const DentistOffices = require('./DentistOffices.js');

const DentistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 39999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'DENTIST',
            enum: ['DENTIST'],
            uppercase: true,
            trim: true
        },
        subType: {
            type: String,
            default: 'DENTISTRY_GENERAL',
            enum: [
                'DENTISTRY_GENERAL',
                'DENTISTRY_MEDICINE',
                'DENTISTRY_STOMATOLOGY',
                'DENTISTRY_ORTHODONTICS',
                'DENTISTRY_PERIODONTICS',
                'DENTISTRY_GENERAL_TRAINEE',
                'DENTISTRY_MEDICINE_TRAINEE',
                'DENTISTRY_STOMATOLOGY_TRAINEE',
                'DENTISTRY_ORTHODONTICS_TRAINEE',
                'DENTISTRY_PERIODONTICS_TRAINEE'
            ],
            uppercase: true,
            trim: true
        },
        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: DentistOffices }]
    },
    {
        timestamps: true
    }
);

DentistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.DentistPractitioners ??
    mongoose.model('DentistPractitioners', DentistPractitionersSchema);
