const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GynecologistOffices = require('./GynecologistOffices.js');
const GynecologistPractitioners = require('./GynecologistPractitioners.js');
const GynecologistTimeslots = require('./GynecologistTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const GynecologistResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: GynecologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: GynecologistOffices
        },
        type: {
            type: String,
            required: true,
            default: 'GYNECOLOGIST',
            enum: ['GYNECOLOGIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: GynecologistTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

GynecologistResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.GynecologistResultCards ??
    mongoose.model('GynecologistResultCards', GynecologistResultCardsSchema);
