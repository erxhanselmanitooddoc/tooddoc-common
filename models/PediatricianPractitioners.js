const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const PediatricianOffices = require('./PediatricianOffices.js');

const PediatricianPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'PEDIATRICIAN',
            enum: ['PEDIATRICIAN'],
            uppercase: true,
            trim: true
        },

        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: PediatricianOffices }]
    },
    {
        timestamps: true
    }
);

PediatricianPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.PediatricianPractitioners ??
    mongoose.model('PediatricianPractitioners', PediatricianPractitionersSchema);
