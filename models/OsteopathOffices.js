const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const geoJSONPointSchema = require('./schemas/GeoJSONPoint.js');
const officeElementsSchema = require('./schemas/OfficeElements.js');
const tooddocElementsSchema = require('./schemas/TooddocElements');
const OsteopathPractitioners = require('./OsteopathPractitioners.js');
const contactElementsSchema = require('./schemas/ContactElements.js');
const addressElementsSchema = require('./schemas/AddressElements.js');

const OsteopathOfficesSchema = new Schema(
    {
        geoLocation: {
            type: geoJSONPointSchema,
            required: true,
            index: '2dsphere'
        },
        canonicalAddress: {
            type: String,
            unique: true,
            required: true,
            trim: true
        },
        name: {
            type: String,
            trim: true
        },
        addressElements: addressElementsSchema,
        contactElements: contactElementsSchema,
        practitionersIds: [{ type: mongoose.Schema.Types.ObjectId, ref: OsteopathPractitioners }],
        officeElements: officeElementsSchema,
        tooddocElements: tooddocElementsSchema
    },
    {
        timestamps: true
    }
);
OsteopathOfficesSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.OsteopathOffices ??
    mongoose.model('OsteopathOffices', OsteopathOfficesSchema);
