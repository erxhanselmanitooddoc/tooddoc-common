const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PsychiatristOffices = require('./PsychiatristOffices.js');
const PsychiatristPractitioners = require('./PsychiatristPractitioners.js');
const PsychiatristTimeslots = require('./PsychiatristTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const PsychiatristResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: PsychiatristPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: PsychiatristOffices
        },
        type: {
            type: String,
            required: true,
            default: 'PSYCHIATRIST',
            enum: ['PSYCHIATRIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: PsychiatristTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

PsychiatristResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.PsychiatristResultCards ??
    mongoose.model('PsychiatristResultCards', PsychiatristResultCardsSchema);
