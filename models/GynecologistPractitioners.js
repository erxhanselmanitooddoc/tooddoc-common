const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tooddocAttributesSchema = require('./schemas/TooddocAttributes');
const inamiAttributesSchema = require('./schemas/InamiAttributes');
const personAttributesSchema = require('./schemas/PersonAttributes');
const GynecologistOffices = require('./GynecologistOffices.js');

const GynecologistPractitionersSchema = new Schema(
    {
        inamiAttributes: inamiAttributesSchema({ min: 10000000, max: 19999999 }),
        personAttributes: personAttributesSchema,
        tooddocAttributes: tooddocAttributesSchema,
        type: {
            type: String,
            required: true,
            default: 'GYNECOLOGIST',
            enum: ['GYNECOLOGIST'],
            uppercase: true,
            trim: true
        },
        officeIds: [{ type: mongoose.Schema.Types.ObjectId, ref: GynecologistOffices }]
    },
    {
        timestamps: true
    }
);

GynecologistPractitionersSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.GynecologistPractitioners ??
    mongoose.model('GynecologistPractitioners', GynecologistPractitionersSchema);
