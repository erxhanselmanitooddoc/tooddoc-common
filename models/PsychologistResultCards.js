const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PsychologistOffices = require('./PsychologistOffices.js');
const PsychologistPractitioners = require('./PsychologistPractitioners.js');
const PsychologistTimeslots = require('./PsychologistTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const PsychologistResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: PsychologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: PsychologistOffices
        },
        type: {
            type: String,
            required: true,
            default: 'PSYCHOLOGIST',
            enum: ['PSYCHOLOGIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: PsychologistTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

PsychologistResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.PsychologistResultCards ??
    mongoose.model('PsychologistResultCards', PsychologistResultCardsSchema);
