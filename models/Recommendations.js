const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RecommendationsSchema = new Schema(
    {
        from: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        to: {
            email: {
                type: String,
                required: true
            },
            firstName: {
                type: String,
                required: true
            },
            lastName: {
                type: String,
                required: true
            }
        }
    },
    {
        timestamps: true
    }
);

module.exports =
    mongoose?.models?.Recommendations ?? mongoose.model('Recommendations', RecommendationsSchema);
