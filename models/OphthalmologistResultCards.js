const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OphthalmologistOffices = require('./OphthalmologistOffices.js');
const OphthalmologistPractitioners = require('./OphthalmologistPractitioners.js');
const OphthalmologistTimeslots = require('./OphthalmologistTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const OphthalmologistResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: OphthalmologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: OphthalmologistOffices
        },
        type: {
            type: String,
            required: true,
            default: 'OPHTHALMOLOGIST',
            enum: ['OPHTHALMOLOGIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: OphthalmologistTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

OphthalmologistResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.OphthalmologistResultCards ??
    mongoose.model('OphthalmologistResultCards', OphthalmologistResultCardsSchema);
