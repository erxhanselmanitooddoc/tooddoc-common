import { capitalize } from '@/utils/helpers';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const practices = require('@/common/utils/practices.json');

const ScheduleDayNewSchema = require('./schemas/ScheduleDayNew');
const ScheduleDayOldSchema = require('./schemas/ScheduleDayOld');
const ScheduleDayOverrideSchema = require('./schemas/ScheduleDayOverride');

const SchedulesSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: practices,
            uppercase: true,
            trim: true
        },
        practitionerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        officeId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        oldSchedule: {
            type: [ScheduleDayOldSchema],
            // required: true,
            validate: [oldScheduleValidator, 'This old schedule is not valid']
        },
        newSchedule: {
            type: {
                monday: { type: ScheduleDayNewSchema },
                tuesday: { type: ScheduleDayNewSchema },
                wednesday: { type: ScheduleDayNewSchema },
                thursday: { type: ScheduleDayNewSchema },
                friday: { type: ScheduleDayNewSchema },
                saturday: { type: ScheduleDayNewSchema },
                sunday: { type: ScheduleDayNewSchema }
            },
            required: true
        },
        // This is for special days
        overrideSchedule: {
            type: [ScheduleDayOverrideSchema]
        }
    },
    {
        timestamps: true,
        toObject: { virtuals: true },
        toJSON: { virtuals: true }
    }
);

SchedulesSchema.index({ practitionerId: 1, officeId: 1 }, { unique: true });

/**
 * Virtual populates for practitioner, office & timeslot
 */
SchedulesSchema.virtual('practitioner', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Practitioners';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'practitionerId',
    foreignField: '_id',
    justOne: true
});
SchedulesSchema.virtual('office', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Offices';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'officeId',
    foreignField: '_id',
    justOne: true
});

const getDynamicModel = (resultName) => {
    return require(`../models/${resultName}`).default;
};

SchedulesSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

function oldScheduleValidator(val) {
    return val.length == 0 || val.length > 6;
}

module.exports = mongoose?.models?.Schedules ?? mongoose.model('Schedules', SchedulesSchema);
