const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhysiotherapistOffices = require('./PhysiotherapistOffices.js');
const PhysiotherapistPractitioners = require('./PhysiotherapistPractitioners.js');
const PhysiotherapistTimeslots = require('./PhysiotherapistTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const PhysiotherapistResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: PhysiotherapistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: PhysiotherapistOffices
        },
        type: {
            type: String,
            required: true,
            default: 'PHYSIOTHERAPIST',
            enum: ['PHYSIOTHERAPIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: PhysiotherapistTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

PhysiotherapistResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.PhysiotherapistResultCards ??
    mongoose.model('PhysiotherapistResultCards', PhysiotherapistResultCardsSchema);
