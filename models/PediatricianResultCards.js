const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PediatricianOffices = require('./PediatricianOffices.js');
const PediatricianPractitioners = require('./PediatricianPractitioners.js');
const PediatricianTimeslots = require('./PediatricianTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const PediatricianResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: PediatricianPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: PediatricianOffices
        },
        type: {
            type: String,
            required: true,
            default: 'PEDIATRICIAN',
            enum: ['PEDIATRICIAN'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: PediatricianTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

PediatricianResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.PediatricianResultCards ??
    mongoose.model('PediatricianResultCards', PediatricianResultCardsSchema);
