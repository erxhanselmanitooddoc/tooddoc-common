const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DermatologistOffices = require('./DermatologistOffices.js');
const DermatologistPractitioners = require('./DermatologistPractitioners.js');
const DermatologistTimeslots = require('./DermatologistTimeslots');
const Schedule = require('./Schedules.js');
const geoJSONPointSchema = require('./schemas/GeoJSONPointNotUnique.js');
const partnerAttributesSchema = require('./schemas/PartnerAttributes');

const DermatologistResultCardsSchema = new Schema(
    {
        practitioner: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no practitioner?'],
            ref: DermatologistPractitioners
        },
        office: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no office?'],
            ref: DermatologistOffices
        },
        type: {
            type: String,
            required: true,
            default: 'DERMATOLOGIST',
            enum: ['DERMATOLOGIST'],
            trim: true
        },
        weightScore: {
            type: Number,
            required: true,
            default: 1,
            min: 0,
            max: 10
        },
        schedule: { type: mongoose.Schema.Types.ObjectId, ref: Schedule },
        geoLocation: {
            type: geoJSONPointSchema,
            index: '2dsphere',
            required: true
        },
        timeslots: [{ type: mongoose.Schema.Types.ObjectId, ref: DermatologistTimeslots }],
        partnerAttributes: [partnerAttributesSchema]
    },
    {
        timestamps: true
    }
);

DermatologistResultCardsSchema.index({ practitioner: 1, office: 1 }, { unique: true });

module.exports =
    mongoose?.models?.DermatologistResultCards ??
    mongoose.model('DermatologistResultCards', DermatologistResultCardsSchema);
