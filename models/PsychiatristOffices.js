const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const geoJSONPointSchema = require('./schemas/GeoJSONPoint.js');
const officeElementsSchema = require('./schemas/OfficeElements.js');
const tooddocElementsSchema = require('./schemas/TooddocElements');
const PsychiatristPractitioners = require('./PsychiatristPractitioners.js');
const contactElementsSchema = require('./schemas/ContactElements.js');
const addressElementsSchema = require('./schemas/AddressElements.js');

const PsychiatristOfficesSchema = new Schema(
    {
        geoLocation: {
            type: geoJSONPointSchema,
            required: true,
            index: '2dsphere'
        },
        canonicalAddress: {
            type: String,
            unique: true,
            required: true,
            trim: true
        },
        name: {
            type: String,
            trim: true
        },
        addressElements: addressElementsSchema,
        contactElements: contactElementsSchema,
        practitionersIds: [
            { type: mongoose.Schema.Types.ObjectId, ref: PsychiatristPractitioners }
        ],
        officeElements: officeElementsSchema,
        tooddocElements: tooddocElementsSchema
    },
    {
        timestamps: true
    }
);
PsychiatristOfficesSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true });
    next();
});

module.exports =
    mongoose?.models?.PsychiatristOffices ??
    mongoose.model('PsychiatristOffices', PsychiatristOfficesSchema);
