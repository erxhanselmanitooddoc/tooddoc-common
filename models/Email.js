const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmailSchema = new Schema(
    {
        source: {
            type: String,
            default: 'hello@tooddoc.com'
        },
        destination: {
            type: [String],
            required: true
        },
        sent: {
            type: Boolean,
            default: false
        },
        due: {
            type: Date,
            required: true
        },
        template: {
            type: String,
            required: true
        },
        templateData: {
            type: Object,
            required: true
        }
    },
    {
        timestamps: true
    }
);

// Template + due + destination = id

EmailSchema.virtual('created').get(function () {
    if (this['_created']) return this['_created'];
    return (this['_created'] = this._id.getTimestamp());
});

module.exports = mongoose.models.Email || mongoose.model('Email', EmailSchema);
