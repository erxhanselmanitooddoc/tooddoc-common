const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const practices = require('@/common/utils/practices.json');
import { capitalize } from '@/utils/helpers';
const Patients = require('./Patients.js');
import { updateOneForType } from '@/common/db/updateFromType';
const geoJSONPointNotUniqueSchema = require('./schemas/GeoJSONPointNotUnique');

// Works for all medical specialties
const AppointmentsSchema = new Schema(
    {
        practitionerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: function () {
                return !this.emergency;
            }
        },
        officeId: {
            type: mongoose.Schema.Types.ObjectId,
            required: function () {
                return !this.emergency;
            }
        },
        timeslotId: {
            type: mongoose.Schema.Types.ObjectId,
            required: function () {
                return !this.emergency;
            }
        },
        patient: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Why no patient?'],
            ref: Patients
        },
        guest: {
            firstName: {
                type: String,
                trim: true
            },
            lastName: {
                type: String,
                trim: true
            },
            email: {
                type: String,
                trim: true
            },
            phone: {
                type: String,
                trim: true
            },
            age: {
                type: Number,
                trim: true
            }
        },
        type: {
            type: String,
            required: true,
            enum: practices,
            uppercase: true,
            trim: true
        },
        subType: {
            type: [String]
        },
        emergency: {
            type: Boolean,
            default: false
        },
        symptoms: {
            type: Schema.Types.Mixed,
            required: function () {
                return this.emergency;
            }
        },
        autoCancelled: {
            type: Boolean,
            default: false
        },
        cancelled: {
            type: Boolean,
            default: false
        },
        likedByPatient: {
            type: Boolean,
            default: false
        },
        geoLocation: {
            type: geoJSONPointNotUniqueSchema,
            required: true
        },
        locality: {
            type: {
                en: String,
                fr: String,
                nl: String,
                de: String
            },
            required: true
        },
        feedback: {
            type: String,
            enum: ['ras', 'absent', 'late', 'unpleasant', 'mail']
        },
        isAlreadyTaken: {
            type: Boolean,
            default: false
        },
        caredByPhone: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true,
        toObject: { virtuals: true },
        toJSON: { virtuals: true }
    }
);

AppointmentsSchema.index({ timeslotId: 1 }, { unique: true, sparse: true });

/**
 * Virtual populates for practitioner, office & timeslot
 */
AppointmentsSchema.virtual('practitioner', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Practitioners';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'practitionerId',
    foreignField: '_id',
    justOne: true
});

AppointmentsSchema.virtual('office', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Offices';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'officeId',
    foreignField: '_id',
    justOne: true
});

AppointmentsSchema.virtual('timeslot', {
    ref: function (doc) {
        const resultName = capitalize(doc.type.toLowerCase()) + 'Timeslots';
        getDynamicModel(resultName);
        return resultName;
    },
    localField: 'timeslotId',
    foreignField: '_id',
    justOne: true
});

const getDynamicModel = (resultName) => {
    return require(`../models/${resultName}`).default;
};

/**
 * Listeners
 */
// AppointmentsSchema.pre('validate', function (next) {
//     return next();
// });

AppointmentsSchema.pre('findOneAndUpdate', function (next) {
    this.setOptions({ runValidators: true, useFindAndModify: true });
    next();
});

AppointmentsSchema.post('save', async function (doc, next) {
    try {
        if (!doc.emergency) {
            await updateOneForType({
                type: doc.type,
                collection: 'Timeslots',
                identifier: { _id: doc.timeslotId },
                update: { appointment: doc._id }
            });
        }
    } catch (error) {
        console.error('ERROR', error);
    }
    next();
});

AppointmentsSchema.post('deleteOne', { document: true, query: true }, async function (next) {
    try {
        await updateOneForType({
            type: 'DENTIST',
            collection: 'Timeslots',
            identifier: { _id: this.timeslotId },
            update: { appointment: null }
        });
    } catch (error) {
        console.error('ERROR', error);
    }
    next();
});

module.exports =
    mongoose?.models?.Appointments ?? mongoose.model('Appointments', AppointmentsSchema);
