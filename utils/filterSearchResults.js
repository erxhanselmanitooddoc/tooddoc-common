const filterSearchResults = async ({
    data,
    radius = 50,
    tri,
    subType,
    languages,
    inamiStatus,
    officeOptions
}) => {
    if (!data || !data.length) {
        return [];
    }

    const options = officeOptions?.map((option) => option.value) ?? [];
    const languagesTransformed = languages?.map((lang) => lang.value) ?? [];

    const selectStatus = (element) =>
        inamiStatus
            ? element?.practitioner?.inamiAttributes?.inamiStatus === inamiStatus?.value
            : element;

    const selectSubSpecialties = (element) =>
        subType && subType.length > 0
            ? subType?.some((sub) =>
                  element?.practitioner?.subType?.includes(sub.value.toUpperCase())
              )
            : element;

    const selectLanguages = (element) =>
        languagesTransformed && languagesTransformed.length > 0
            ? languagesTransformed?.some((lang) =>
                  element?.practitioner?.personAttributes?.languages?.includes(lang)
              )
            : element;

    const selectOfficeOptions = (element) =>
        options && options.length > 0
            ? options?.every(
                  (option) =>
                      element?.office?.officeElements &&
                      Object.keys(element?.office?.officeElements)?.includes(option) &&
                      element?.office?.officeElements[option]
              )
            : element;

    const filterRadius = (element) => element?.spatialDist <= radius;

    const filters = [
        selectStatus,
        selectSubSpecialties,
        selectLanguages,
        selectOfficeOptions,
        filterRadius
    ];

    const filteredData = data.filter((v) => filters.every((f) => f(v)));

    const sortedFilteredData = tri
        ? filteredData.sort((a, b) => {
              return ['spatialdist', 'temporaldist'].includes(tri.value.toLowerCase())
                  ? a[tri.value] - b[tri.value]
                  : b[tri.value] - a[tri.value];
          })
        : filteredData;

    return sortedFilteredData || [];
};

export default filterSearchResults;
